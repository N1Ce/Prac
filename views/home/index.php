<div class="container">
    <div class="row pt-2">
        <div class="col-md-4">
            <?php mess::showMes() ?>
            <form action="<?=URL ?>home/form">
                <div class="form-group">
                    <label for="text">Город:</label>
                    <input type="text" class="form-control" id="text" name="text">
                </div>
                <div class="form-group">
                    <label for="date">Дата:</label>
                    <input type="date" class="form-control" id="date" name="date">
                </div>
                <button type="submit" class="btn btn-default">Submit</button>
            </form>
        </div>
        <div class="col-md-7">
                <?php
                if($data != ''):
                    for ($i = 0; $i < count($data); $i++):
                    if($if) {
                        $t = json_decode($data[$i][0]);
                    } else {
                        $t = json_decode($data[$i]);
                    }
                        switch($t->weather[0]->description) {
                            case 'clear sky':
                                $sky = 'чистое небо';
                                break;
                            case 'few clouds':
                                $sky = 'несколько облаков';
                                break;
                            case 'scattered clouds':
                                $sky = 'рассеянные облака';
                                break;
                            case 'broken clouds':
                                $sky = 'разбитые облака';
                                break;
                            case 'shower rain':
                                $sky = 'сильный дождь';
                                break;
                            case 'light rain':
                                $sky = 'легкий дождь';
                                break;
                            case 'thunderstorm':
                                $sky = 'гроза';
                                break;
                            case 'snow':
                                $sky = 'снег';
                                break;
                            case 'mist':
                                $sky = 'туман';
                                break;
                            default:
                                $sky = $t->weather[0]->description;
                        }
                ?>
                        <div class="card">
                            <button class="btn bg-dark text-white d-flex justify-content-between" data-toggle="collapse" data-target="#block<?=$i ?>">
                        <span class="d-flex align-items-center"><?=date('d-m-Y', $t->dt) ?>
                            <img src="http://openweathermap.org/img/w/<?=$t->weather[0]->icon ?>.png" class="ml-2 mr-2">
                            <span class="badge badge-success"><?=round($t->main->temp - 273.15, 1, PHP_ROUND_HALF_UP) ?>&#176;</span>
                            <span class="badge badge-primary text-white ml-2"><?=$sky ?></span>
                        </span>
                                <span class="align-self-center"><?=date('H:i', $t->dt) ?></span>
                            </button>
                            <div id="block<?=$i ?>" class="collapse">
                                <table class="card-body table">
                                    <thead class="thead-light">
                                    <tr>
                                        <th>Название</th>
                                        <th>Значение</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>Температура:</td>
                                        <td><span class="badge badge-success"><?=round($t->main->temp - 273.15, 1, PHP_ROUND_HALF_UP) ?></span> &#176;C</td>
                                    </tr>
                                    <tr>
                                        <td>Влажность:</td>
                                        <td><span class="badge badge-success"><?=$t->main->humidity ?></span> %</td>
                                    </tr>
                                    <tr>
                                        <td>Мин. Температура:</td>
                                        <td><span class="badge badge-success"><?=round($t->main->temp_min - 273.15, 1, PHP_ROUND_HALF_UP) ?></span> &#176;С</td>
                                    </tr>
                                    <tr>
                                        <td>Макс. Температура:</td>
                                        <td><span class="badge badge-success"><?=round($t->main->temp_max - 273.15, 1, PHP_ROUND_HALF_UP) ?></span> &#176;С</td>
                                    </tr>
                                    <tr>
                                        <td>Атмосферное давление:</td>
                                        <td><span class="badge badge-success"><?=$t->main->pressure ?></span> hPa</td>
                                    </tr>
                                    <tr>
                                        <td>Атмосферное давление на уровне моря:</td>
                                        <td><span class="badge badge-success"><?=$t->main->sea_level ?></span> hPa</td>
                                    </tr>
                                    <tr>
                                        <td>Атмосферное давление на уровне земли:</td>
                                        <td><span class="badge badge-success"><?=$t->main->grnd_level ?></span> hPa</td>
                                    </tr>
                                    <tr>
                                        <td>Ветер:</td>
                                        <td><img src="http://www.clker.com/cliparts/7/U/O/H/c/K/big-blue-up-arrow-md.png" style="width: 25px; transform: rotate(<?=$t->wind->deg ?>deg);"> <span class="badge badge-success"><?=round($t->wind->speed) ?></span> м/с</td>
                                    </tr>
                                    <tr>
                                        <td>Облачность:</td>
                                        <td><span class="badge badge-success"><?=$t->clouds->all ?></span> %</td>
                                    </tr>
                                    <tr>
                                        <td>Погодные условия:</td>
                                        <td><span class="badge badge-success"><?=$sky ?></span></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                <?php
                    endfor;
                endif;
                ?>
        </div>
	</div>
</div>

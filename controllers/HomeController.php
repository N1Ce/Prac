<?php
require_once ('models/PogodaModel.php');
class HomeController {
    protected $model;
	function __construct(){
		$this->model = new Pogoda();
	}
	function index()
    {
	    $this->model->createTable();
		View::render('home/index', '', '');
	}
	function form()
    {
        $soob = new mess();
        if($_GET['text'] == '') {
            $soob->addMes('Вы не велли город');
            View::render('home/index', '', '');
        } elseif($_GET['date'] == '') {
            $soob->addMes('Вы не велли дату');
            View::render('home/index', '', '');
        } else {
            $mas = $this->model->showDate($_GET['date'], $_GET['text']);
            if($mas) {
                $soob->addMes('Погода есть в базе!');
                View::render('home/index', $mas, true);
            } else {
                $result = curl($_GET['text']);
                if($result->cod == '404') {
                    $soob->addMes('Такого города нет!');
                    View::render('home/index', '', '');
                } else {
                    $value = sortDate($result);
                    $this->model->addPogoda($value, $_GET['text']);
                    $pogoda = $value[$_GET['date']];
                    $soob->addMes('Погоды нет в базе!');
                    View::render('home/index', $pogoda, false);
                }
            }
        }
    }
}

?>